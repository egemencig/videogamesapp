//
//  API.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class API {
    
    static let sharedManager = API()
    private var sessionManager = SessionManager()
    private init() { }
    

    fileprivate let encoding = JSONEncoding.default

    func getListGames(completion: @escaping (GetListResponse?, Error?) -> Void) {
        let endpoint = "https://rawg-video-games-database.p.rapidapi.com/games"
        let headers = [
            "x-rapidapi-key": "423d9300edmshc9def4cad72baa5p1c324bjsn4621afce8c81",
            "x-rapidapi-host": "rawg-video-games-database.p.rapidapi.com"
        ]
        request(endpoint: .getList, method: .get, headers: headers, endpointURL: endpoint).responseObject {
            (response: DataResponse<GetListResponse>) in self.handle(response: response, completion: completion)
        }
    }
    
    func getGameDetails(id:String, completion: @escaping (DetailsResponse?, Error?) -> Void) {
        let endpoint = "https://rawg-video-games-database.p.rapidapi.com/games/\(id)"
        let headers = [
            "x-rapidapi-key": "423d9300edmshc9def4cad72baa5p1c324bjsn4621afce8c81",
            "x-rapidapi-host": "rawg-video-games-database.p.rapidapi.com"
        ]

        request(endpoint: .getDetails, method: .get, headers: headers, endpointURL: endpoint).responseObject {
            (response: DataResponse<DetailsResponse>) in self.handle(response: response, completion: completion)
        }
    }
}

extension API {
    
    fileprivate func handle<T>(response: DataResponse<T>, completion: @escaping (T?, Error?) -> Void) {
        
        if response.error != nil {
            completion(nil, response.error)
        }
        if let responseResultValue = response.result.value {
            completion(responseResultValue, nil)
        }
        else {
            completion(nil, response.result.error)
        }
    }
}

extension API {
    
    fileprivate func request(endpoint: Endpoint,method: HTTPMethod,headers: HTTPHeaders , endpointURL: String) -> DataRequest {
        
        return sessionManager.request(endpointURL,
                                      method: method,
                                      encoding: API.sharedManager.encoding,
                                      headers: headers)
    }
    
    enum Endpoint {
        case getList
        case getDetails
    }
}

