//
//  TabBarController.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.backgroundColor = UIColor.customGray()
        tabBar.barTintColor = UIColor.customGray()
        tabBar.selectedImageTintColor = UIColor.black
    }
    
}
