//
//  GameListViewController.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit

class GameListViewController: BaseViewController {
    
    @IBOutlet weak var gameListVM: GameListViewModel!
    
    var gameListArray = [ResultModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestStatus = .pending
        setupNavigationBar()
        gameListVM.delegate = self
        gameListVM.getGameList()
        gameListVM.setSearchBar()
        gameListVM.setupTableView()
        gameListVM.setScrollView()
        gameListVM.pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControl.Event.valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Games"
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.customGray()
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.setStatusBar(backgroundColor: UIColor.customGray())
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationItem.searchController = gameListVM.searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        definesPresentationContext = true
    }
    
    
    @objc func openDetails(tapGestureRecognizer: UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let index = tappedImage.tag
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController {
            if let gameId = gameListArray[index].id {
                viewController.id = String(gameId)
            }
            self.navigationController?.pushViewController(viewController, animated: false)
        }
        
    }
    
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(gameListVM.pageControl.currentPage) * gameListVM.scrollView.frame.size.width
        gameListVM.scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
}


extension GameListViewController: GameListDelegate {
    func changed(status: BaseViewController.RequestStatus, array: [ResultModel]) {
        requestStatus = status
        self.gameListArray = array
        switch status {
        case .completed(let error):
            if error != nil {
                print("error")
            }
            else {
                gameListVM.noResultLabel.isHidden = true
                gameListVM.tableView.reloadData()
            }
        default: break
        }
        
        setFirstImageArray(array: array)
    }
    
    func setFirstImageArray(array: [ResultModel]) {
        gameListVM.scrollView?.isPagingEnabled = true
        gameListVM.pageControl?.numberOfPages = array.count
        gameListVM.scrollView.subviews.forEach({$0.removeFromSuperview()})
        gameListVM.scrollView.backgroundColor = .clear
        gameListVM.scrollView?.contentSize = CGSize(width: Int(UIScreen.main.bounds.size.width) * array.count , height: Int(gameListVM.scrollView.frame.height))
        
        for index in 0..<array.count {
            let imageView = UIImageView()
            imageView.frame = CGRect.init(x: Int(UIScreen.main.bounds.size.width) * index , y: 0, width:Int(UIScreen.main.bounds.size.width)  , height: Int(gameListVM.scrollView.bounds.size.height))
            if let url = array[index].backgroundImage, let imageUrl = URL(string: url) {
                imageView.sd_setImage(with: imageUrl, completed: nil)
            }
            gameListVM.scrollView.addSubview(imageView)
            let tapImage = UITapGestureRecognizer(target: self, action: #selector(openDetails(tapGestureRecognizer:)))
            imageView.addGestureRecognizer(tapImage)
            imageView.isUserInteractionEnabled = true
            imageView.tag = index
        }
        
    }
    
    func didSelect(didSelect: ResultModel) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController {
            if let gameId = didSelect.id {
                viewController.id = String(gameId)
            }
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    
}


