//
//  GameListViewModel.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import Foundation
import UIKit

protocol GameListDelegate {
    func changed(status: BaseViewController.RequestStatus, array: [ResultModel])
    func didSelect(didSelect: ResultModel)
}

class GameListViewModel: NSObject {
    
    var delegate: GameListDelegate?
    var gameListArray: [ResultModel] = []
    var searchResults: [ResultModel] = []
    var getListResponse: GetListResponse?
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var noResultLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    func setupTableView() {
        tableView.register(UINib(nibName: "GameListTableViewCell", bundle: nil), forCellReuseIdentifier: "gameCell")
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
    }
    
    func getGameList() {
        API.sharedManager.getListGames { (response, error) in
            DispatchQueue.main.async {
                if let gameList = response?.results {
                     let games = Array(gameList.suffix(gameList.count - 3))
                    self.gameListArray = games
                    let firstGameArray = Array(gameList.prefix(3))
                    self.delegate?.changed(status: .completed(error), array: firstGameArray)
                }
            }
        }
    }
    
    
    func setSearchBar() {
        
        searchController.searchBar.delegate = self
        searchController.searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 175/255, green: 175/255, blue: 175/255, alpha: 1.0), NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 13.0)!])
        searchController.searchBar.searchTextField.textColor = .black
        searchController.searchBar.searchTextField.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
        searchController.searchBar.searchTextField.font = UIFont(name: "HelveticaNeue-Medium", size: 15.0)
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = .black
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.barTintColor = .clear
        searchController.searchBar.barStyle = .default
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.barTintColor = .white
        searchController.searchBar.isTranslucent = true
        searchController.searchBar.isHidden = false
        
        for view in searchController.searchBar.subviews {
            for subview in view.subviews where subview.isKind(of: NSClassFromString("UISearchBarBackground")!) {
                subview.alpha = 0
            }
        }
    }
    
    func setScrollView() {
        scrollView.delegate = self
        pageControl?.pageIndicatorTintColor = .lightGray
        pageControl?.currentPageIndicatorTintColor = .black
        pageControl?.hidesForSinglePage = true
        scrollView?.showsHorizontalScrollIndicator = false
    }
    
    
}

extension GameListViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchController.isActive == true && searchController.searchBar.text != "" {
            self.delegate?.didSelect(didSelect: searchResults[indexPath.row])
        }
        else {
            self.delegate?.didSelect(didSelect: gameListArray[indexPath.row])
        }
    }
    
}

extension GameListViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameCell", for: indexPath) as! GameListTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        var gameCellViewModel = GameCellViewModel()
        if searchController.isActive == true && searchController.searchBar.text != "" {
             gameCellViewModel = GameCellViewModel(resultModel: searchResults[indexPath.row])
        }
        else {
             gameCellViewModel = GameCellViewModel(resultModel: gameListArray[indexPath.row])
        }
        
        cell.dataSource = gameCellViewModel
        cell.reloadData()
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive == true && searchController.searchBar.text != "" {
            return searchResults.count
        }
        else {
            return gameListArray.count

        }
    }
}

extension GameListViewModel: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        tableView.reloadData()
        scrollView.isHidden = false
        pageControl.isHidden = false
        noResultLabel.isHidden = true

    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

        scrollView.isHidden = true
        pageControl.isHidden = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 2 {
            searchResults = gameListArray.filter({ (ResultModel) -> Bool in
                let match = ResultModel.name?.range(of: searchText, options: .caseInsensitive)
                return (match != nil)
            })

            tableView.reloadData()
            
            if searchResults.count == 0 {
                noResultLabel.isHidden = false
            }
            else {
                noResultLabel.isHidden = true
            }
        }
        else {
            noResultLabel.isHidden = true
        }
        
        
    }
    
}


extension GameListViewModel: UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        
        pageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
}
