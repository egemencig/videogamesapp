//
//  GameCellViewModel.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import Foundation

class GameCellViewModel: NSObject {
    var resultModel: ResultModel?
    
    init(resultModel: ResultModel = ResultModel()) {
        self.resultModel = resultModel
    }
    
    func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMMM d, yyyy"
        return  dateFormatter.string(from: date!)
    }
}

extension GameCellViewModel: GamesTableViewCellDataSource {
    func imageForCell(_ cell: GameListTableViewCell) -> URL {
        if let url = resultModel?.backgroundImage ,let imageUrl = URL(string: url) {
            return imageUrl
        }
        else {
            return URL(string: "")!
        }
    }
    
    func nameForCell(_ cell: GameListTableViewCell) -> String {
        if let gameName = resultModel?.name {
            return gameName
        }
        else {
            return ""
        }
    }
    
    func ratingForCell(_ cell: GameListTableViewCell) -> String {
        if let rating = resultModel?.rating, let released = resultModel?.released {
            let date = convertDateFormater(released)
            return "\(rating) - \(date)"
        }
        else {
            return ""
        }
    }
    
    
}


