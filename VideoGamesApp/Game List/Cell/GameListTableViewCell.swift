//
//  GameListTableViewCell.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit
import SDWebImage

protocol GamesTableViewCellDataSource {
    func imageForCell(_ cell: GameListTableViewCell) -> URL
    func nameForCell(_ cell: GameListTableViewCell) -> String
    func ratingForCell(_ cell: GameListTableViewCell) -> String

}

class GameListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var gameRatingLabel: UILabel!

    var dataSource: GamesTableViewCellDataSource?
    
    func reloadData() {
        gameImage.sd_setImage(with: dataSource?.imageForCell(self), completed: nil)
        gameNameLabel.text = dataSource?.nameForCell(self)
        gameRatingLabel.text = dataSource?.ratingForCell(self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        gameImage.contentMode = .scaleAspectFill
    }

    
}
