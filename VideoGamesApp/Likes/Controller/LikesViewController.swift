//
//  LikesViewController.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit

class LikesViewController: BaseViewController {
    
    @IBOutlet weak var likeListVM: LikeListViewModel!
    @IBOutlet weak var tableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        requestStatus = .pending
        setupTableView()
        setupNavigationBar()
        likeListVM.delegate = self
        likeListVM.getLikeList()
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Likes"
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.customGray()
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.setStatusBar(backgroundColor: UIColor.customGray())
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "GameListTableViewCell", bundle: nil), forCellReuseIdentifier: "gameCell")
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
    }
    

}
extension LikesViewController: LikeListDelegate {
    func changed(status: BaseViewController.RequestStatus) {
        requestStatus = status
        switch status {
        case .completed(let error):
            if error != nil {
                print("error")
            }
            else {
               tableView.reloadData()
            }
        default: break
        }
    }
    
    func didSelect(didSelect: ResultModel) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController {
            if let gameId = didSelect.id {
                viewController.id = String(gameId)
            }
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
}
