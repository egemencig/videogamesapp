//
//  LikeViewModel.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import Foundation
import UIKit

protocol LikeListDelegate {
    func changed(status: BaseViewController.RequestStatus)
    func didSelect(didSelect: ResultModel)
}

class LikeListViewModel: NSObject {

    var delegate: LikeListDelegate?
    var likeListArray: [ResultModel] = []
    
    var getListResponse: GetListResponse?
    
    func getLikeList() {
        
        API.sharedManager.getListGames { (response, error) in
            if let gameList = response?.results {
                
                for index in 0..<gameList.count where index % 2 == 1 {
                    gameList[index].isLike = true
                }
                
                self.likeListArray = gameList.filter({$0.isLike == true})
                self.delegate?.changed(status: .completed(error))
            }
        }
    }
    
}

extension LikeListViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelect(didSelect: likeListArray[indexPath.row])
    }
    
}

extension LikeListViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameCell", for: indexPath) as! GameListTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        let gameCellViewModel = GameCellViewModel(resultModel: likeListArray[indexPath.row])
        cell.dataSource = gameCellViewModel
        cell.reloadData()
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likeListArray.count
    }
}
