//
//  DetailsTableViewCell.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameDescLabel: UILabel!
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var gameReleaseLabel: UILabel!
    @IBOutlet weak var gameMetacriticLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        gameImage.contentMode = .scaleToFill
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
