//
//  DetailsViewController.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit
import SDWebImage

class DetailsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!

    var id: String = String()
    var detailsModel: DetailsResponse = DetailsResponse()

    override func viewDidLoad() {
        super.viewDidLoad()
        getDetails()
        setupTableView()
        setupNavigationBar()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "DetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "detailCell")
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
    }
    
    func getDetails() {
        API.sharedManager.getGameDetails(id: id) { (response, error) in
            if let details = response {
                self.detailsModel = details
                self.tableView.reloadData()
            }
        }
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Details"
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.customGray()
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
 
    
    func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMMM d, yyyy"
        return  dateFormatter.string(from: date!)
    }
  

}

extension DetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailsTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none        
        
        cell.gameDescLabel.text = detailsModel.desc
        cell.gameNameLabel.text = detailsModel.name
        cell.gameDescLabel.sizeToFit()

        if let url = detailsModel.backgroundImage, let imageUrl = URL(string: url) {
            cell.gameImage.sd_setImage(with: imageUrl, completed: nil)
        }
        
        if let metacritic = detailsModel.metacritic, let released = detailsModel.released {
            cell.gameMetacriticLabel.text = "Metacritic Rate: \(metacritic)"
            cell.gameReleaseLabel.text = "Released Date: \(convertDateFormater(released))"
        }
        
        return cell

    }
}
