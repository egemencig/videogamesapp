//
//  UIColorExtension.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import UIKit

extension UIColor {
    
    class func customGray() -> UIColor {
        return UIColor(red: 236/255, green: 235/255, blue: 238/255, alpha: 1.0)
    }

}
