//
//  GetListResponse.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import Foundation
import ObjectMapper
import RealmSwift

class GetListResponse: BaseResponseModel {
    
    var count: Int?
    var next: String?
    var results: [ResultModel]?
    var seoTitle: String?
    var seoDescription: String?
    var seoKeywords: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        count    <- map["count"]
        next    <- map["next"]
        results    <- map["results"]
        seoTitle    <- map["seo_title"]
        seoDescription    <- map["seo_description"]
        seoKeywords    <- map["seo_keywords"]
    }
    
}
