//
//  ResultModel.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//

import Foundation
import ObjectMapper
import RealmSwift

class ResultModel: BaseResponseModel {
    
    var id: Int?
    var slug: String?
    var name: String?
    var released: String?
    var tba: Bool?
    var backgroundImage: String?
    var rating: Double?
    var ratingTop: Int?
    var ratingsCount: Int?
    var metaCritic: Int?
    var playTime: Int?
    var isLike: Bool?

    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id    <- map["id"]
        slug    <- map["slug"]
        name    <- map["name"]
        released    <- map["released"]
        tba    <- map["tba"]
        backgroundImage    <- map["background_image"]
        rating    <- map["rating"]
        ratingTop    <- map["rating_top"]
        ratingsCount    <- map["ratings_count"]
        metaCritic    <- map["metacritic"]
        playTime    <- map["playtime"]
    }
    
}
