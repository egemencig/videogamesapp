//
//  DetailsResponse.swift
//  VideoGamesApp
//
//  Created by Egemen Çığ on 2.03.2021.
//



import Foundation
import ObjectMapper
import RealmSwift

class DetailsResponse: BaseResponseModel {
    
    var name: String?
    var desc: String?
    var released: String?
    var metacritic: Int?
    var backgroundImage: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        name    <- map["name"]
        desc    <- map["description"]
        released    <- map["released"]
        metacritic    <- map["metacritic"]
        backgroundImage    <- map["background_image"]
    }
    
}
